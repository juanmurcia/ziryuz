<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'clients';

    public $fillable = [
        "name",
        "address",
        "phone",
        "email",
        "nit",
        "active",
    ];

    protected $casts = [
        "name" => "string|Min:4",
        "address" => "string|Min:4",
        "phone" => "string|Min:4",
        "email" => "string|Min:4",
        "nit" => "string|Min:4",
        "active" => "boolean",
    ];

    public static $rules = [
        'txtName' => 'required',
        'txtNit' => 'required|unique:clients.nit',
        'txtEmail' => 'required|email|unique:clients.email',
    ];

    public static $fields = [
        'txtNombre' => 'Nombre',
        'txtDireccion' => 'Direccion',
        'txtTelefono' => 'Telefono',
    ];
}
