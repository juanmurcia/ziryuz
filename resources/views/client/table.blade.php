

<div class="right_col" role="main">
    <div>
        <div class="page-title clearfix">
            <div style="float: left">
                <h1>Personas</h1>
            </div>
        </div>
        <div class="x_panel">
            <table id="tbPerson" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>T.Doc</th>
                    <th>Documento</th>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Direccion</th>
                    <th>Email</th>
                    <th>Teléfonos</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>